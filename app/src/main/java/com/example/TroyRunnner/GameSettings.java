package com.example.TroyRunnner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GameSettings extends AppCompatActivity {
    public static  boolean Easy= true;
    public static  boolean Medium= false;
    public static  boolean Impossible= false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_settings);



        final Button Toggle;
        Toggle = (Button)findViewById(R.id.Toggle);

        Toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Easy && !Medium && !Impossible){
                    Easy=false;
                    Medium=true;
                    Toggle.setText("Medium");
                }else if (!Easy && Medium && !Impossible){
                    Medium=false;
                    Impossible=true;
                    Toggle.setText("Impossible");
                }else if(!Easy && !Medium && Impossible){
                    Impossible=false;
                    Easy=true;
                    Toggle.setText("Easy");

                }

            }
        });
    }
}
