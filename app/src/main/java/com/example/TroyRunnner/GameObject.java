package com.example.TroyRunnner;

import android.graphics.Rect;


public abstract class GameObject {

    protected float x;
    protected float y;
    protected int dy;
    protected int width;
    protected int height;

    public float getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public Rect getBounds() {
        return new Rect(Math.round(x) + 1, Math.round(y) + 1, Math.round(x) + 1 + width, Math.round(y) + 1 + height);
    }

}
