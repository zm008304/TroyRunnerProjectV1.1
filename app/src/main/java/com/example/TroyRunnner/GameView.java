package com.example.TroyRunnner;

import android.content.Context;
import android.content.SharedPreferences;

import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.SurfaceHolder;

import java.util.ArrayList;
import java.util.Random;

import static com.example.TroyRunnner.GameSettings.Easy;
import static com.example.TroyRunnner.GameSettings.Impossible;
import static com.example.TroyRunnner.GameSettings.Medium;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {


    private MainThread thread;
    private Player Player;
    private ArrayList<Missiles> missiles;
    private long missileStartTime;
    private int screenHeight = getHeight();
    private int screenWidth = getWidth();
    private Random rand = new Random();
    private boolean newgamecreated;
    private long startReset;
    private boolean reset;
    private boolean dissapear;
    private boolean started;
    private int HighScore;
    private boolean firsttime = true;

    public GameView(Context context) {
        super(context);

        getHolder().addCallback(this);


        setFocusable(true);

    }

    ;

    /**
     * My overridden Methods
     */


    /**
     * @param event captures the users event
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                if (!Player.getPlaying() && newgamecreated && reset) {
                    Player.setPlaying(true);
                    Player.setUp(true);
                    firsttime = false;
                }
                if (Player.getPlaying()) {

                    if (!started) started = true;
                    reset = false;
                    Player.setUp(true);

                    Player.x = event.getRawX() - 80;

                }
            }
            case MotionEvent.ACTION_MOVE: {

                Player.x = event.getRawX() - 80;
                Player.y = event.getRawY() - 80;


            }
        }


        if (event.getAction() == MotionEvent.ACTION_UP) {
            Player.setUp(false);
            return true;
        }

        return super.onTouchEvent(event);
    }


    /**
     * @param canvas drawing on the selected canvas ie our game canvas
     */
    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            final int savedState = canvas.save();

            if (!dissapear) {
                Player.draw(canvas);
            }
            screenHeight = getHeight();
            for (Missiles m : missiles) {
                m.draw(canvas);
            }
            drawText(canvas);
            canvas.restoreToCount(savedState);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {


        Player = new Player(BitmapFactory.decodeResource(getResources(), R.drawable.falconspaceship), 500, screenHeight - 20, 120, 182);
        //load  Highscore
        SharedPreferences prefs = this.getContext().getSharedPreferences("HighScore", Context.MODE_PRIVATE);
        HighScore = prefs.getInt("key", 0); //0 is the default value
        missiles = new ArrayList<Missiles>();
        missileStartTime = System.nanoTime();

        thread = new MainThread(getHolder(), this);
        thread.setRunning(true);
        thread.start();

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        int counter = 0;
        while (retry && counter < 1000) {
            counter++;
            SharedPreferences prefs = this.getContext().getSharedPreferences("HighScore", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("key", HighScore);
            editor.commit();
            try {
                thread.setRunning(false);
                thread.join();
                retry = false;

                thread = null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    public void update() {
        if (Player.getPlaying() && Easy) { // game logic to be exectued if game setting is on easy

            Player.update();


            //add missiles on timer
            long missileElapsed = (System.nanoTime() - missileStartTime) / 1000000;
            if (missileElapsed > (2000 - Player.getScore() / 4)) {

                System.out.println("making missile");
                //first missile always goes down the middle
                if (missiles.size() == 0) {
                    missiles.add(new Missiles(BitmapFactory.decodeResource(getResources(), R.drawable.
                            enemyship), 45, 15, 150, 192, Player.getScore()));
                    for (int i = 0; i < missiles.size(); i++) {
                        //update missile
                        missiles.get(i).setSpeed(7);
                    }
                } else {

                    missiles.add(new Missiles(BitmapFactory.decodeResource(getResources(), R.drawable.
                            enemyship), (int) (rand.nextDouble() * (screenHeight)), 15, 150, 192, Player.getScore()));
                }

                //reset timer
                missileStartTime = System.nanoTime();
            }

            for (int i = 0; i < missiles.size(); i++) {
                //update missile
                missiles.get(i).update();

                if (collision(missiles.get(i), Player)) {
                    missiles.remove(i); //remove missile if it has collided
                    Player.setPlaying(false);
                    //  System.out.println("Collision Detected"); was used for debugging
                    break;
                }

                //remove missiles if they off the screen
                if (missiles.get(i).getY() > screenHeight) {
                    missiles.remove(i);
                    break;
                }


            }
        } else if (Player.getPlaying() && Medium) { // game logic to be exectued if game setting is on medium

            Player.update();


            //add missiles on timer
            long missileElapsed = (System.nanoTime() - missileStartTime) / 1000000;
            if (missileElapsed > (1000 - Player.getScore() / 4)) {

                System.out.println("making missile");
                //first missile always goes down the middle
                if (missiles.size() == 0) {
                    missiles.add(new Missiles(BitmapFactory.decodeResource(getResources(), R.drawable.
                            enemyship), 45, 15, 150, 192, Player.getScore()));
                    for (int i = 0; i < missiles.size(); i++) {
                        //update missile
                        missiles.get(i).setSpeed(11);
                    }
                } else {

                    missiles.add(new Missiles(BitmapFactory.decodeResource(getResources(), R.drawable.
                            enemyship), (int) (rand.nextDouble() * (screenHeight)), 15, 150, 192, Player.getScore()));
                }

                //reset timer
                missileStartTime = System.nanoTime();
            }

            for (int i = 0; i < missiles.size(); i++) {
                //update missile
                missiles.get(i).update();

                if (collision(missiles.get(i), Player)) {
                    missiles.remove(i);
                    Player.setPlaying(false);
                    //  System.out.println("Collision Detected"); was used for debugging
                    break;
                }

                //remove missiles if they off the screen
                if (missiles.get(i).getY() > screenHeight) {
                    missiles.remove(i);
                    break;
                }


            }
        } else if (Player.getPlaying() && Impossible) {   // game logic to be exectued if game setting is on IMpossible

            Player.update();


            //add missiles on timer
            long missileElapsed = (System.nanoTime() - missileStartTime) / 1000000;
            if (missileElapsed > (800 - Player.getScore() / 4)) {

                System.out.println("making missile");
                //first missile always goes down the middle
                if (missiles.size() == 0) {
                    missiles.add(new Missiles(BitmapFactory.decodeResource(getResources(), R.drawable.
                            enemyship), 45, 15, 150, 192, Player.getScore()));
                    for (int i = 0; i < missiles.size(); i++) {
                        //update missile
                        missiles.get(i).setSpeed(15);
                    }
                } else {

                    missiles.add(new Missiles(BitmapFactory.decodeResource(getResources(), R.drawable.
                            enemyship), (int) (rand.nextDouble() * (screenHeight)), 15, 150, 192, Player.getScore()));
                }

                //reset timer
                missileStartTime = System.nanoTime();
            }

            for (int i = 0; i < missiles.size(); i++) {
                //update missile
                missiles.get(i).update();

                if (collision(missiles.get(i), Player)) {
                    missiles.remove(i);
                    Player.setPlaying(false);
                    //  System.out.println("Collision Detected"); was used for debugging
                    break;
                }

                //remove missiles if they off the screen
                if (missiles.get(i).getY() > screenHeight) {
                    missiles.remove(i);
                    break;
                }


            }
        } else {
            if (!reset)      //if nothing else meets those conditions it must mean the game has to be reset
            {
                newgamecreated = false;
                startReset = System.nanoTime();
                reset = true;
                dissapear = true;

                if (Player.getScore() > HighScore) {
                    HighScore = Player.getScore();
                }


            }


            long resetElapsed = (System.nanoTime() - startReset) / 1000000;

            if (resetElapsed > 2500 && !newgamecreated) {
                newgame();
            }
        }
    }


    // return true if there is a collison

    public boolean collision(GameObject a, GameObject b) {
        if (Rect.intersects(a.getBounds(), b.getBounds())) {
            return true;
        }
        return false;
    }


    public void newgame() {
        dissapear = false;
        missiles.clear();

        Player.setY(screenHeight / 2);

        newgamecreated = true;

        Player.resetScore();


    }


    /**
     * @param canvas drawing on our game canvas
     */
    public void drawText(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setTextSize(30);
        paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        canvas.drawText("Score: " + (Player.getScore()), 10, screenHeight - 10, paint);
        canvas.drawText("HighScore: " + HighScore, 30, 60, paint);

        if (!Player.getPlaying() && newgamecreated && reset && firsttime) {
            Paint paint1 = new Paint();
            paint1.setColor(Color.WHITE);
            paint1.setTextSize(40);
            paint1.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
            canvas.drawText("PRESS TO START", 30, 30, paint1);
            canvas.drawText("Click to Play Again!", 600, 350, paint1);

        }
        if (!Player.getPlaying() && newgamecreated && reset && !firsttime) {
            Paint paint1 = new Paint();
            paint1.setColor(Color.WHITE);
            paint1.setTextSize(40);
            paint1.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
            canvas.drawText("Click to Play Again!", 500, 350, paint1);
            canvas.drawText("Click to Play Again!", 600, 350, paint1);
            canvas.drawText("HighScore: " + HighScore, 500, 400, paint);

        }
    }


}

