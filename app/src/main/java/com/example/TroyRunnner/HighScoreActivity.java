package com.example.TroyRunnner;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HighScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int HighScore1;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_score);
        SharedPreferences prefs = getSharedPreferences("HighScore", Context.MODE_PRIVATE);
        HighScore1 = prefs.getInt("key", 0); //0 is the default value

        TextView HS;
        HS = (TextView) findViewById(R.id.textView3);

        HS.setText("" + HighScore1);


    }


}
