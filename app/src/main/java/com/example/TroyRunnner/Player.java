package com.example.TroyRunnner;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.animation.Animation;

public class Player extends GameObject {

    private Bitmap playerImage;
    private int score;
    private boolean playing;
    private long startTime;
    private boolean up;

    /**
     * @param image specfiying what image the object player is going to reprsent
     */
    public Player(Bitmap image, int x, int y, int w, int h) {
        playerImage = image;
        super.x = x;
        super.y = y;
        y = 500;
        width = w;
        height = h;
        score = 0;
        dy = 0;
    }
    /**
     * @param canvas drawing on our game canvas
     */
    public void draw(Canvas canvas) {
        canvas.drawBitmap(playerImage, x, y, null);
    }

    public void update() {
        long elapsed = (System.nanoTime() - startTime) / 1000000;
        if (elapsed > 100) {
            score++;
            startTime = System.nanoTime();
        }
    }

    public int getScore() {
        return score;
    }

    public boolean getPlaying() {
        return playing;
    }

    public void setPlaying(boolean b) {
        playing = b;
    }

    public void resetScore() {
        score = 0;
    }
    public void setUp(boolean b){up = b;}

}
