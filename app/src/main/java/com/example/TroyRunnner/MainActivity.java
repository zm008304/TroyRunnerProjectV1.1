package com.example.TroyRunnner;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import static com.example.TroyRunnner.MainThread.gameView;

public class MainActivity extends Activity {

    private GameView GameView;
    private MainThread Thread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        Button StartButton;
        Button HSButton;
        Button GameSettings;
        HSButton = (Button) findViewById(R.id.HSButton);
        StartButton = (Button) findViewById(R.id.StrtButton);
        GameSettings = (Button) findViewById(R.id.GameSettingsBtn);
        GameView = new GameView(this);

        StartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(GameView);
            }
        });

        HSButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openHighscores();
            }
        });

        GameSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGameSettings();
            }
        });


    }

    private void openGameSettings() {
        Intent GS = new Intent(this, GameSettings.class);
        startActivity(GS);
    }

    private void openHighscores() {

        Intent HS = new Intent(this, HighScoreActivity.class);
        startActivity(HS);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        //replaces the default 'Back' button action
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {

            Intent hs = new Intent(this, MainActivity.class);
            startActivity(hs);

        }
        return true;
    }
}
