package com.example.TroyRunnner;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import java.util.Random;

public class Missiles extends GameObject {
    private int speed;
    private int score;
    private Random rand = new Random();
    private Bitmap resource;

    /**
     * @param res specfiying what image the object missiles is going to represent
     */
    public Missiles(Bitmap res, int x, int y, int w, int h, int s) {
        super.x = x;
        super.y = y;
        width = w;
        height = h;

        speed = 7 + (int) (rand.nextDouble() * score / 30);
        if (speed > 40) speed = 40;

        resource = res;

    }

    public void update() {

        y += speed;
    }

    /**
     * @param canvas drawing on our game canvas
     */
    public void draw(Canvas canvas) {
        canvas.drawBitmap(resource, x, y, null);
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
